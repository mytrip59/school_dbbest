/*Создать класс java.TextInput, который содержит абстрактный метод Validate и StringBuilder переменную с введённой строкой, от него нужно унаследовать
        java.IntegerTextInput, который валидирует, что строка — это целое число. И от java.IntegerTextInput унаследовать java.FloatTextInput, который валидирует,
        что число целое или дробное с точкой  - для этого нужно вызвать  base.Validate() проверить, что число целое и если не целое тогда проверять
        с точкой ли оно*/

public class Lesson12_abstract_inheritance {
    public static void main(String[] args) {
    new FloatTextInput().validate(TextInput.StringBuilder);
    }
}

abstract class TextInput {
    static protected String StringBuilder = "10.3";
    abstract void validate (String sting);
}

class IntegerTextInput extends TextInput {
    @Override
    void validate(String sting) {
        try {
            int i3 = Integer.parseInt(sting);
            System.out.println(i3 + " - Correct Int format");
        } catch (NumberFormatException e) {
            System.err.println("Incorrect Int format");
        }
    }
}

class FloatTextInput extends IntegerTextInput {
    @Override
    void validate(String sting) {
        try {
            int i3 = Integer.parseInt(sting);
            System.out.println(i3 + " - Correct Int format");
        } catch (NumberFormatException e) {
            try {
                Double db = Double.parseDouble(sting);
                System.out.println(db + " - Correct Double format");
            } catch (NumberFormatException ee) {
                System.err.println("Incorrect Double format");
            }
        }
    }
}