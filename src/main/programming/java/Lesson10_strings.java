/*Дан пусть к файлу в файловой системе. Необходимо найти имя файла без расширения и вывести на экран.*/
/*Previous step: need to create txt file e:\test_file with any text*/

import java.io.*;

public class Lesson10_strings {
    public static void main(String[] args) throws IOException {
        File f = new File("e:\\test_file");
        BufferedReader fin = new BufferedReader(new FileReader(f));
        String line;
        System.out.println("Print File " + f.getName());
        while ((line = fin.readLine()) != null) System.out.println(line);
    }
}

