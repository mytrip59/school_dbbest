package java;/*
        написать 2класса студент и школьник,в которых реализовать интерфейс учиться.
        Студент/школьник могут иметь имя,фамилию,возраст,пол,факультет,специальность(для школьника № школы,класс).
        В каждом классе реализовать несколько конструкторов,для случаев когда по студенту/школьнику заполнена вся информация либо пропущена(например имя,возраст).
        Создать несколько объектов студентов и школьников и узнать где они учатся.
*/

import java.util.ArrayList;
import java.util.List;

interface StudyI {
    String getStudyPlace ();

    String getName();
    String getSurname();
}

public class Lesson_StudyInterface {
    public static void main(String[] args) {
        Learner student1 = new Student("Ivan", "Ivanov");
        Learner student2 = new Student("Vasia", "Vasiliev");
        Learner pupil1 = new Pupil("Pavel", "Pavlov", "17 school", "10A");
        Learner pupil2 = new Pupil("Oleg", "Olegrov");

        List <Learner> learnerArray = new ArrayList();
        learnerArray.add(student1);
        learnerArray.add(student2);
        learnerArray.add(pupil1);
        learnerArray.add(pupil2);

        learnerArray.forEach(s->System.out.println(s.getName() + " " + s.getSurname() + ". Study plase is " + s.getStudyPlace()));

    }
}

abstract class Learner implements StudyI {
    public Learner(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    private String name;
    private String surname;
    private int age;
    private String sex;

    public Learner(String name, String surname, int age, String sex) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }
}

class Student extends Learner {
    public String getFaculty() {
        return faculty;
    }

    private String faculty;
    private String speciality;

    public Student(String name, String surname, int age, String sex, String faculty, String speciality) {
        super(name, surname, age, sex);
        this.faculty = faculty;
        this.speciality = speciality;
    }

    public Student(String name, String surname) {
        super(name, surname);
        this.faculty = "Faculty PO";
        this.speciality = "programmer";
    }

    @Override
    public String getStudyPlace() {
        return getFaculty();
    }

}

class Pupil extends Learner {
    private String schoolNumber;
    private String classNumber;

    public Pupil(String name, String surname, int age, String sex, String schoolNumber, String classNumber) {
        super(name, surname, age, sex);
        this.schoolNumber = schoolNumber;
        this.classNumber = classNumber;
    }
    public Pupil(String name, String surname, String schoolNumber, String classNumber) {
        super(name, surname);
        this.schoolNumber = schoolNumber;
        this.classNumber = classNumber;
    }

    public Pupil(String name, String surname) {
        super(name, surname);
        this.schoolNumber = "99";
        this.classNumber = "1A";
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public String getClassNumber() {
        return classNumber;
    }

    @Override
    public String getStudyPlace() {
        return getSchoolNumber() + ", " + getClassNumber();
    }
}
