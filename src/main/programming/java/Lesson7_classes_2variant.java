// Perhaps gender field better to move to class Animal. Because it's an animal characteristic
// For serious task for price better to use BigDecimal

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Lesson7_classes_2variant {
    public static void main(String argrs[]) {
        Animal wolf1 = new Animal("wolf", "female", 10);
        Animal wolf2 = new Animal("wolf", "female", 5);
        Animal penguin1 = new Animal("penguin", "male", 3);


        AnimalDelivery animalDelivery1 = new AnimalDelivery(wolf1,2, new GregorianCalendar(2021,0,1));
        AnimalDelivery animalDelivery2 = new AnimalDelivery(wolf2,3, new GregorianCalendar(2021,0,2));
        AnimalDelivery animalDelivery3 = new AnimalDelivery(penguin1,4, new GregorianCalendar(2021,0,3));

        System.out.println("Sum price for all female: " + animalDelivery1.sumPriceBySex("male"));
    }
}

class AnimalDelivery {
    private static List<AnimalDelivery> listDeliveries = new ArrayList<>();
    private int amountDelivery;
    private Animal animal;
    private int oneDeliveryCost;
    private GregorianCalendar dateDelivery;

    public AnimalDelivery(Animal animal, int amountDelivery, GregorianCalendar dateDelivery) {
        this.amountDelivery = amountDelivery;
        this.animal = animal;
        this.dateDelivery = dateDelivery;
        this.oneDeliveryCost = getOneDeliveryCost();
        listDeliveries.add(this);
    }

    private int getOneDeliveryCost() {
        return this.animal.getPrice()*this.getAmountDelivery();
    }

    public int getAmountDelivery() {
        return amountDelivery;
    }

    public Animal getAnimal() {
        return animal;
    }

    public int sumPriceBySex(String sex) {
        int sum = 0;
        for (AnimalDelivery animalDelivery : listDeliveries) {
            if (animalDelivery.getAnimal().getSex().equals(sex)) {
                sum = sum + animalDelivery.oneDeliveryCost;
            }
        }
        return sum;
    }
}

class Animal {
    private String type;
    private String sex;
    private int price;

    public Animal(String type, String sex, int price) {
        this.type = type;
        this.sex = sex;
        this.price = price;

    }

    public String getSex() {
        return sex;
    }

    public int getPrice() {
        return price;
    }


}

