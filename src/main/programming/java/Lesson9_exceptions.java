import java.util.Scanner;

public class Lesson9_exceptions {
    public static void main(String argrs[]) {
        System.out.println("Input any number, but not equal 0");
        Scanner sc = new Scanner(System.in);
        int firstNumber = sc.nextInt();
        System.out.println("Input any number");
        int secondNumber = sc.nextInt();
        try {
            if(firstNumber==0) throw new Exception("The first number is: " + firstNumber);
            System.out.println("The first number divided by the second is: " + firstNumber/secondNumber);
        } catch (Exception e) {
            System.out.println("The input number is out of acceptable range");
        }

    }
}
