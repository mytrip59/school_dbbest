import java.lang.reflect.Array;
import java.util.*;

public class Lesson11_collections {
    public static void main(String argrs[]) {
        String myString =
                "Happy new year\n" +
                "May we all have a vision now and then\n";

        String [] myArray = myString.split("[^a-zA-Z]+");
        List <String >myCollection = new ArrayList();
        Collections.addAll(myCollection,myArray);
        System.out.println("Сollection has: " + myCollection.size() + " elements");
        myCollection.forEach(s->System.out.println(s));

        System.out.println("Input any number of collection element from 0 till " + (myCollection.size()-1) + ": ");
        Scanner sc = new Scanner(System.in);
        int collectionElementNumber = sc.nextInt();
        if (collectionElementNumber < 0 || collectionElementNumber > myCollection.size()-1){
            System.out.println("You input incorrect number");
            System.exit(3);
        }
        System.out.println("Input the collection direction: 1-forward or 2-back: ");
        int forwardOrBack = sc.nextInt();
        for (int x = collectionElementNumber; ;){
            if (forwardOrBack == 1) {
                System.out.println(myCollection.get(x));
                x++;
                if ( x >= myCollection.size()) System.exit(1);

            } else if (forwardOrBack == 2){
                System.out.println(myCollection.get(x));
                x--;
                if ( x < 0) System.exit(2);
            } else {
                System.out.println("You input incorrect number");
                System.exit(3);
            }
        }

    }
}
