import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;
import java.util.Scanner;

public class Lesson3_arrays {
    int firstArraySize;
    int secondArraySize;

    public static void main(String argrs[]) {
        String [][] myArray;
        Lesson3_arrays lesson3_arrays = new Lesson3_arrays();

        myArray = lesson3_arrays.createStringArray();
        lesson3_arrays.fillAraryByStrings(myArray);
        lesson3_arrays.printArrayCellByNumber(myArray);

    }

    public String [][] createStringArray (){
        System.out.print("To create a two-dimensional array, enter the first integer in the range from 1 to 9: ");
        firstArraySize = checkOnlyCorrectInt(1,9);
        System.out.print("ДTo create a two-dimensional array, enter the second integer in the range from 1 to 9: ");
        secondArraySize = checkOnlyCorrectInt(1,9);

        return new String[firstArraySize][secondArraySize];
    }

    public void fillAraryByStrings (String [][] myArray){
        for (int i=0;i<myArray.length;i++){
            for (int j=0;j<myArray[i].length;j++){
                myArray[i][j] = generatingRandomAlphabeticString();
            }
        }
    }

    public void printArrayCellByNumber (String [][]myArray){
        int firstArraySizeMinusOne = firstArraySize - 1;
        int secondArraySizeMinusOne = secondArraySize - 1;
        int firstArraySizeDisplay;
        int secondArraySizeDisplay;

        System.out.print("To display the cell value, enter the row number in the limit of previously created array (numbering from 0) to " + firstArraySizeMinusOne + " : ");
        firstArraySizeDisplay = checkOnlyCorrectInt(0 ,firstArraySizeMinusOne);
        System.out.print("To display the cell value, enter the column number in the limit of previously created array (numbering from 0) to  " + secondArraySizeMinusOne + " : ");
        secondArraySizeDisplay = checkOnlyCorrectInt(0, secondArraySizeMinusOne);

        String myString = myArray[firstArraySizeDisplay][secondArraySizeDisplay];
        System.out.println("The value of the matrix cell with numbering, starting from 0, " + firstArraySizeDisplay + " и " + secondArraySizeDisplay + " = " + myString);
    }

    public int checkOnlyCorrectInt (int startCounting, int topLimit) {
        Scanner sc = new Scanner(System.in);
        int i;
        if(sc.hasNextInt()) {
            i = sc.nextInt();
            if (i >= startCounting && i <= topLimit ) {
                return i;
            } else {
                System.out.println("You entered an integer, but not in the range from 0 to "  + topLimit);
                System.exit(1);
                return -1;
            }
        } else {
            System.out.println("You entered not an integer");
            System.exit(1);
            return -1;
        }
    }

    public String generatingRandomAlphabeticString() {
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        System.out.println(generatedString);
        return generatedString;
    }

}
