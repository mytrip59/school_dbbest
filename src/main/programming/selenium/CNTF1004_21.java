import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static java.lang.Thread.sleep;


@Test
public class CNTF1004_21 {
    private WebDriver driver;
    private final By bodyXPath = By.xpath("/html/body");

    private final Map<String, String []> CNTF1004MapReference = new HashMap<String, String []>() {
        {
            String grupoCnt1 = "0";
            String GrupoCnt2 = "0005024";
            String Calibre = "0";
            String DtEntrada = "2015/07/25";
            String TipoContador1 = "10";
            String TipoContador2 = "VOLUMETRICO";
            String Fabricante1 = "9";
            String Fabricante2 = "C.A.L.";
            String Modelo1 = "2";
            String Modelo2 = "FICTICIO";
            String Dimensao = "P";
            String NrFabrica1 = "1500";
            String NrFabrica2 = "643540";
            String Situacao1 = "2";
            String Situacao2 = "RETIRADO";
            String Situacao3 = "2003/09/29";
            String NrFabricaLow1 = " ";
            String NrFabricaLow2 = " ";
            String SituacaoLow1 = " ";
            String SituacaoLow2 = " ";
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[16]", new String []{grupoCnt1, "GrupoCnt1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[15]", new String []{GrupoCnt2, "GrupoCnt2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[12]", new String []{Calibre, "Calibre"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[9]", new String []{DtEntrada, "DtEntrada"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[14]", new String []{TipoContador1, "TipoContador1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[13]", new String []{TipoContador2, "TipoContador2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[11]", new String []{Fabricante1, "Fabricante1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[10]", new String []{Fabricante2, "Fabricante2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[8]", new String []{Modelo1, "Modelo1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[7]", new String []{Modelo2, "Modelo2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[6]", new String []{Dimensao, "Dimensao"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[5]", new String []{NrFabrica1, "NrFabrica1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[4]", new String []{NrFabrica2, "NrFabrica2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[3]", new String []{Situacao1, "Situacao1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[2]", new String []{Situacao2, "Situacao2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[5]/input[1]", new String []{Situacao3, "Situacao3"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[6]/input[4]", new String []{NrFabricaLow1, "NrFabricaLow1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[6]/input[3]", new String []{NrFabricaLow2, "NrFabricaLow2"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[6]/input[2]", new String []{SituacaoLow1, "SituacaoLow1"});
            put("//*[@id='main']/div/div/div/ng-include/div/form/div[2]/div/div/fieldset/div[6]/input[1]", new String []{SituacaoLow2, "SituacaoLow2"});
        }
    };


    @BeforeClass
    void launchChrome() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
//chromeOptions.addArguments(/*"--start-maximized", */"--test-data");
        chromeOptions.addArguments("--start-maximized");
        if (driver == null) {
            driver = new ChromeDriver(chromeOptions);
        }

        driver.get("http://192.168.88.213:8083/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Test (priority = 1)
    void test_00_IdentificationOnSite (){
        WebElement loginField = driver.findElement(By.xpath("//*[@id='i0116']"));
        loginField.sendKeys("epalglobaladmin@epaldev.onmicrosoft.com");

        WebElement NextButton = driver.findElement(By.xpath("//*[@id='idSIButton9']"));
        NextButton.click();

        WebElement passwordField = driver.findElement(By.xpath("//*[@id='i0118']"));
        passwordField.sendKeys("pastEPAL432101");

        By signInButton = By.xpath("//*[@id='idSIButton9']");
        waitTillNotException(signInButton, 10).click();

        // not work correctly!
/*        Wait wait = new FluentWait<>(driver).withMessage("Element was not found").withTimeout(30, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));*/

        // not work correctly
/*        WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(signInButton)).click();*/

        By staySignedInButtonNo = By.xpath("//*[@id='idBtn_Back']");
        waitTillNotException(staySignedInButtonNo, 10).click();

        By selectCompanyDropDownMenuBy = By.xpath("//*[@id='ngdialog1']/div[2]/div/form/fieldset/div[2]/div/div/div/div[2]/select");
        Select variableName = new Select(waitTillNotException(selectCompanyDropDownMenuBy, 20));
        variableName.selectByVisibleText("EPAL");

        WebElement selectCompanyOkButton = driver.findElement(By.xpath("//*[@id='ngdialog1']/div[2]/div/form/fieldset/div[3]/div/div/a/span"));
        selectCompanyOkButton.click();

        waitTillNotException(bodyXPath,20);
        Assert.assertEquals("AQUA NEXT", driver.getTitle(), "Title is not 'AQUA NEXT'");
    }

    @Test(priority = 2, dependsOnMethods = {"test_1_1_IdentificationOnSite"})
    void test_00_openForm() {
        By contadoresMenu = By.xpath("//*[@id='sidebar']/div/div[1]/ul/li[7]/a/span[2]");
        waitTillNotException(contadoresMenu, 10).click();

        By consultasMenu = By.xpath("//*[@id='menu_287']");
        waitTillNotException(consultasMenu, 10).click();

        By contadoresSecundariosMenu = By.xpath("//*[@id='menu_CNTF1004']");
        waitTillNotException(contadoresSecundariosMenu, 10).click();

        By formHeader = By.xpath("//*[@id=\"main\"]/div/div/div/ng-include/div/form/div[2]/epal-form-header/div/div/div[4]/span");
        String formHeaderValue = waitTillNotException(formHeader,20).getText();
        Assert.assertEquals(formHeaderValue, "CNTF1004", "Form is not 'CNTF1004'");
    }

    @Test(priority = 3, dependsOnMethods = {"test_1_2_openForm"})
    void Test_01_search_all_data_default() {
        boolean testFailed = true;
        // Steps
        waitTillNotException(bodyXPath,20);
        driver.findElement(By.tagName("body")).sendKeys(Keys.F8);

        // create clone for reference Map
        Map<String, String[]> CNTF1004MapActual = new HashMap<>(CNTF1004MapReference);

        // Clear only value for the actual Map
/*        for (Map.Entry<String, String[]> item : CNTF1004MapActual.entrySet()) {
            CNTF1004MapActual.put(item.getKey(), null);
        }*/
        CNTF1004MapActual.replaceAll((k, v) -> null);

        // get value from page to actual Map
        for (Map.Entry<String, String[]> item : CNTF1004MapReference.entrySet()) {
            By byItemKey = By.xpath(item.getKey());
            String valueItem = driver.findElement(byItemKey).getAttribute("value");
            CNTF1004MapActual.put(item.getKey(), new String []{valueItem, null});
            // System.out.println("key: " + item.getKey() + " . Value: " + CNTF1004MapActual.get(item.getKey())[0]);
        }

        for (Map.Entry<String, String []> item : CNTF1004MapReference.entrySet()) {
            String referenceValue=null;
            String actualValue=null;
            String fieldName=null;
            try {
                referenceValue = item.getValue()[0];
                actualValue = CNTF1004MapActual.get(item.getKey())[0];
                fieldName = item.getValue()[1];
                Assert.assertEquals(actualValue, referenceValue);
            } catch (AssertionError e){
                testFailed = false;
                System.out.println("Field " + fieldName + " is not equal. Reference = '" + referenceValue
                        + "' .Actual result = '" +  actualValue + "'");
            }
        }
        if (!testFailed) {
            throw new AssertionError ("Test 2.1 is failed");
        }

    }

    @AfterClass
    void closeWebdriver() {
        driver.quit();
    }

    WebElement waitTillNotException(By by, int timeSecWait) {
        WebElement webElement = null;
        for (int i = 0; i < 2 * timeSecWait; i++) {
            try {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                webElement = driver.findElement(by);
            } catch (StaleElementReferenceException e) {
                System.out.println("500 msec");

            }
        }
        return webElement;
    }

}
