import java.util.Scanner;

public class Lesson3_arrays_short {
    public static void main(String argrs[]) {
        String [][] myArray = new String[][]{
                {"a1","b1"},
                {"a2","b2"},
                {"a3","b3"}};
        System.out.println("Input the number from 0 to " + (myArray.length-1));
        Scanner sc = new Scanner(System.in);
        int firstNumber = sc.nextInt();
        System.out.println("Input the number from 0 to " + (myArray[0].length -1));
        int secondNumber = sc.nextInt();
        System.out.println(myArray[firstNumber][secondNumber]);
    }

}
