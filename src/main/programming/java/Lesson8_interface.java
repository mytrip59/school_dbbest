import java.util.ArrayList;
import java.util.List;

public class Lesson8_interface {
    public static void main(String argrs[]) {
        IShape triangle1 = new Triangle(1, 1, 3); // 5
        IShape square1 = new Square(2, 2, 3, 3); // 10
        IShape hexagon1 = new Hexagon(1, 1, 1, 2, 2, 2); // 9

        List<IShape> allShapes = new ArrayList<>();
        allShapes.add(triangle1);
        allShapes.add(square1);
        allShapes.add(hexagon1);

        allShapes.forEach(s->System.out.println("Name = " + s.getName() + ". getPerimeter = " + s.getPerimeter() + ". getAmountCorners = " + s.getAmountCorners()));

    }
}

interface IShape {
    double getPerimeter();
    double getAmountCorners();
    String getName ();
}

class Triangle implements IShape {
    String name = "Triangle";
    private int a, b, c;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        return a + b + c;
    }

    @Override
    public double getAmountCorners() {
        return 3;
    }

    @Override
    public String getName() {
        return name;
    }
}

class Square implements IShape {
    private String name = "Square";
    private int a, b, c, d;

    public Square(int a, int b, int c, int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public double getPerimeter() {
        return a + b + c + d;
    }

    @Override
    public double getAmountCorners() {
        return 4;
    }

    @Override
    public String getName() {
        return name;
    }
}

class Hexagon implements IShape {
    private String name = "Hexagon";
    private int a, b, c, d, e, f;

    public Hexagon(int a, int b, int c, int d, int e, int f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    @Override
    public double getPerimeter() {
        return a + b + c + d + e + f;
    }

    @Override
    public double getAmountCorners() {
        return 6;
    }

    @Override
    public String getName() {
        return name;
    }
}