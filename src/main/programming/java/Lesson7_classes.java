import java.util.ArrayList;
import java.util.List;

public class Lesson7_classes {
    public static void main(String argrs[]) {
        new ZooShop("wolf", "female", 7, 2);
        new ZooShop("wolf", "male", 3, 2);
        new ZooShop("penguin", "male", 5, 4);
        new ZooShop("dog", "male", 2, 9);

        System.out.println("Sum price for all male: " + ZooShop.sumPriceBySex("male"));
    }
}

class ZooShop {
    private String animal;
    private String sex;
    private long priceForOne;
    private long amount;
    private static List <ZooShop> allZooProducts = new ArrayList<>();

    public ZooShop(String animal, String sex, long priceForOne, long amount) {
        this.animal = animal;
        this.sex = sex;
        this.priceForOne = priceForOne;
        this.amount = amount;
        allZooProducts.add(this);
    }

    static public long sumPriceBySex(String sex) {
        long sum = 0;
        for (ZooShop zooShop : allZooProducts) {
            if (zooShop.getSex().equals(sex)) {
                sum = sum + zooShop.getPriceForOne() * zooShop.getAmount();
            }
        }
        return sum;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public long getPriceForOne() {
        return priceForOne;
    }

    public void setPriceForOne(long priceForOne) {
        this.priceForOne = priceForOne;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
